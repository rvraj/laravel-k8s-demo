#!/bin/bash
set -e
cp -rfp /usr/src/files/. /var/www/html/app
rm -rf /usr/src/files
exec "$@";
